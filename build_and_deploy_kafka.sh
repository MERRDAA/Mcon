docker run -d -p 2181:2181 --name zookeeper jplock/zookeeper
sleep 1
docker run -d -p 9092:9092 --name kafka --link zookeeper:zookeeper ches/kafka
sleep 1
ZK_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' zookeeper)
sleep 1
KAFKA_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' kafka)
sleep 1
docker run --rm ches/kafka kafka-topics.sh --create --topic 404_all --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181
sleep 1
docker run --rm ches/kafka kafka-topics.sh --create --topic 404_windows --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181
sleep 1
docker run --rm ches/kafka kafka-topics.sh --create --topic 404_alerts --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181
sleep 1

