package group401.group_401.repositories;

import group401.group_401.domain.Room;
import group401.group_401.domain.Sensor;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface RoomRepository extends MongoRepository<Room, String>{
	List<Room> findAll();

    Room getById(String id);
    
    Room getByRoomId(int roomId);

    void deleteById(String id);
   
    long deleteByRoomId(int roomId);
}
