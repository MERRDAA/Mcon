package group401.group_401.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import group401.group_401.domain.User;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String>{

	List<User> findAll();
    
    User getByUserId(int fakeId);

    User getById(String id);
    
    void deleteById(String id);

    long deleteByUserId(int fakeId);

	User getByUsername(String username);
}

