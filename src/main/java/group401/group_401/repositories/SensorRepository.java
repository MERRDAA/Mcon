package group401.group_401.repositories;

import group401.group_401.domain.Sensor;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;
public interface SensorRepository extends MongoRepository<Sensor, String>{

	List<Sensor> findAll();

    Sensor getById(String id);
    
    Sensor getByFakeId(int fakeId);

    void deleteById(String id);

    long deleteByFakeId(int fakeId);

}
