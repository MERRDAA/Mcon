package group401.group_401.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import listener.KafkaTopicListener;

@Configuration // this will tell Spring that this is a configuration class
@EnableKafka  // this will tell the Spring-Kafka integration that you want to talk to Kafka
public class KafkaConsumerConfig {

	
    //there are some values that you need to configure in order to consume messages from Kafk
    public Map<String, Object> consumerConfigs() {
    	String kafkaUrl = isJUnitTest() ? "deti-engsoft-10.ua.pt:9092" : "deti-engsoft-01.ua.pt:9092";

        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaUrl);
        //props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                //"172.17.0.4:9092");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
        		StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
        		LongDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "group401.group_401.sensores");

        return props;
    }

    public ConsumerFactory consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory factory = new ConcurrentKafkaListenerContainerFactory<>();
        
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    @Bean
    public KafkaTopicListener receiver() {
        return new KafkaTopicListener();
    } 
    
    public static boolean isJUnitTest() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<StackTraceElement> list = Arrays.asList(stackTrace);
        for (StackTraceElement element : list) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }           
        }
        return false;
    }
}
