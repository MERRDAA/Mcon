package group401.group_401;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import group401.group_401.domain.User;
import group401.group_401.domain.User.UserType;
import group401.group_401.repositories.RoomRepository;
import group401.group_401.repositories.UserRepository;

@SpringBootApplication
public class App implements CommandLineRunner {
	
//	@Bean
//    public BCryptPasswordEncoder bCryptPasswordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
	
	@Autowired
	private UserRepository userService;

    public static void main(String[] args) {
    	SpringApplication.run(App.class, args);
    }

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
	}
    
//	@Override
//	public void run(String... args) throws Exception {
//		userService.deleteAll();
//		userService.save(new User(1, "João Branquinho", "joaobranquinho@ua.pt", UserType.ADMIN));
//		userService.save(new User(2, "Luis", "piorquemerda@viseu.pt", UserType.REGULAR));
//		userService.save(new User(3, "Tiago Ramalho", "mili9ivinti2@beira.pt", UserType.REGULAR));
//	}
}
