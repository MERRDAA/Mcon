package group401.group_401.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import group401.group_401.domain.Room;
import group401.group_401.domain.Sensor;
import group401.group_401.domain.User;
import group401.group_401.repositories.UserRepository;
//import group401.group_401.services.UserService;

@RestController
public class UserController {

	@Autowired
	private UserRepository userService;

//	private BCryptPasswordEncoder bCryptPasswordEncoder;
//
//	/*
//	 * @Autowired public void setUserService(UserService userService) {
//	 * this.userService = userService; }
//	 */
//
//	public UserController(UserRepository applicationUserRepository,
//			BCryptPasswordEncoder bCryptPasswordEncoder) {
//		this.userService = applicationUserRepository;
//		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
//	}

	@RequestMapping(value = "/api/users", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> users(@RequestParam(value = "_end", defaultValue = "10", required = false) int end,
			@RequestParam(value = "_order", defaultValue = "DESC", required = false) String order,
			@RequestParam(value = "_sort", defaultValue = "id", required = false) String id,
			@RequestParam(value = "_start", defaultValue = "0", required = false) int start) {

		List<User> users = null;
		if (order == "ASC")
			users = userService.findAll(PageRequest.of(start, end, Sort.Direction.ASC, id)).getContent();
		else
			users = userService.findAll(PageRequest.of(start, end, Sort.Direction.DESC, id)).getContent();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Expose-Headers", "x-total-count");
		headers.add("x-total-count", String.valueOf(users.size()));

		return new ResponseEntity<>(users, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/users/{id}", method = RequestMethod.GET, produces = "application/json")
	public User getUser(@PathVariable(value = "id") int id) {
		return userService.getByUserId(id);
	}

	@RequestMapping(value = "/api/users/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public String delUser(@PathVariable(value = "id") int id) {
		userService.deleteByUserId(id);
		return "user deleted";

	}

//	@PostMapping("/api/sign-up")
//	public void signUp(@RequestBody User user) {
//		user.setPass(bCryptPasswordEncoder.encode(user.getPass()));
//		userService.save(user);
//	}

	/*
	 * Para acrescentar depois
	 * 
	 * @RequestMapping(value="/users",method = RequestMethod.POST, produces =
	 * "application/json") public String createRoom( @RequestParam(value="name")
	 * String name,
	 * 
	 * @RequestParam(value="location") String location,
	 * 
	 * @RequestParam(value="sensor") int sensor ){ List<Room> x =
	 * roomService.findAll(); int newID; if(x.size() == 0) newID = 1; else newID =
	 * x.get(x.size()-1).getRoomId()+1;
	 * 
	 * Sensor s = sensorRepository.getByFakeId(sensor);
	 * 
	 * if (s == null) return "Sensor does not exist";
	 * 
	 * roomService.save(new Room(newID, name, location, sensor)); return
	 * "room created";
	 * 
	 * }
	 */

}
