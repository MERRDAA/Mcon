package group401.group_401.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import group401.group_401.domain.Sensor;
import group401.group_401.repositories.SensorRepository;
//import group401.group_401.services.SoundRecordService;

@RestController
public class SensorController{
    
	@Autowired
	private SensorRepository sensorService;

	/*@Autowired
    public void setSoundRecordService(SoundRecordService soundRecordService) {
        this.soundRecordService = soundRecordService;
    }	*/

    @RequestMapping(value="/api/sensors", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> sensors(    		
    		@RequestParam(value="_end", defaultValue="10", required=false) int end, 
			@RequestParam(value="_order", defaultValue="DESC", required=false) String order, 
			@RequestParam(value="_sort", defaultValue="id", required=false) String id,
			@RequestParam(value="_start", defaultValue="0", required=false) int start){
    	
    	
    	List<Sensor> sensors = null;
    	if(order == "ASC")
    		sensors = sensorService.findAll(PageRequest.of(start, end, Sort.Direction.ASC, id)).getContent();
    	else
    		sensors = sensorService.findAll(PageRequest.of(start, end, Sort.Direction.DESC, id)).getContent();
    	for (Sensor s: sensors) {
    	    s.setId("" + s.getFakeId());
    	}
    	HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Expose-Headers", "x-total-count");
    	headers.add("x-total-count", String.valueOf(sensors.size()));
        return new ResponseEntity<>(sensors, headers, HttpStatus.OK);

    }

    
    @RequestMapping(value="/api/sensors/{id}", method = RequestMethod.GET, produces = "application/json")
    public Sensor sensors(@PathVariable(value="id") String id){

    	Sensor s = sensorService.getByFakeId(Integer.parseInt(id));
    	s.setId("" + s.getFakeId());
		return s;

		
    }
    
    
    @RequestMapping(value="/api/sensors", method = RequestMethod.POST, produces = "application/json")
    public Sensor createSensor(@RequestBody Sensor s){
    	
    	List<Sensor> x = sensorService.findAll();
    	int newID;
    	if(x.size() == 0)
    		newID = 1;
    	else 
    		newID = x.get(x.size()-1).getFakeId()+1;
    	s.setFakeId(newID);
    	
    	
    	sensorService.save(s);
    	s.setId("" + s.getFakeId());
    	
    	
    	return s;
    }
    
    @RequestMapping(value="/api/sensors/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public Sensor delSensor(@PathVariable(value="id") int id){
		sensorService.deleteByFakeId(id);
		
		return new Sensor();
    }
    
    @RequestMapping(value="/api/sensors/{id}",method = RequestMethod.PUT, produces = "application/json")
    public Sensor updateSensor(@RequestBody Sensor r, @PathVariable(value="id") int id){
    	
    	Sensor s = sensorService.getByFakeId(id);
    	if(s != null) {
	    	s.setType(r.getType());
	    	s.setSid(r.getSid());
	    	sensorService.save(s);
	    	
	    	s.setId("" + s.getFakeId());
	    	return s;
    	}
    	else
    		return new Sensor();
        
    }
}
