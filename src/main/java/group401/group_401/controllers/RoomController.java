package group401.group_401.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import group401.group_401.domain.Room;
import group401.group_401.domain.Sensor;
import group401.group_401.repositories.RoomRepository;
import group401.group_401.repositories.SensorRepository;
//import group401.group_401.services.RoomService;

@RestController
public class RoomController {
    
	@Autowired
	private RoomRepository roomService;
	@Autowired
	private SensorRepository sensorRepository;

/*	@Autowired
    public void setRoomService(RoomService roomService) {
        this.roomService = roomService;
    }	*/
	
    @RequestMapping(value="/api/rooms", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> rooms(
    		@RequestParam(value="_end", defaultValue="10", required=false) int end, 
			@RequestParam(value="_order", defaultValue="DESC", required=false) String order, 
			@RequestParam(value="_sort", defaultValue="id", required=false) String id,
			@RequestParam(value="_start", defaultValue="0", required=false) int start){

    	List<Room> rooms = null;
    	if(order == "ASC")
    		rooms = roomService.findAll(PageRequest.of(start, end, Sort.Direction.ASC, id)).getContent();
    	else
    		rooms = roomService.findAll(PageRequest.of(start, end, Sort.Direction.DESC, id)).getContent();

    	for (Room s: rooms) {
    	    s.setId("" + s.getRoomId());
    	}
    	HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Expose-Headers", "x-total-count");
    	headers.add("x-total-count", String.valueOf(rooms.size()));
        return new ResponseEntity<>(rooms, headers, HttpStatus.OK);
    }
    
    @RequestMapping(value="/api/rooms/{id}", method = RequestMethod.GET, produces = "application/json")
    public Room sensors(@PathVariable(value="id") int id){
    	
    	Room s = roomService.getByRoomId(id);
    	s.setId("" + s.getRoomId());
		return s;
    }

    @RequestMapping(value="/api/rooms/{id}",method = RequestMethod.PUT, produces = "application/json")
    public Room editRoom(@RequestBody Room r, @PathVariable(value="id") int id){
    	System.out.println(r);
    	Room s = roomService.getByRoomId(id);
    	if(s != null) {
	    	s.setName(r.getName());
	    	s.setLocation(r.getLocation());
	    	Sensor sensor = sensorRepository.getByFakeId(Integer.parseInt(r.getId()));
	    	if (sensor == null)
	    		return new Room();
	    	s.setSensor(sensor.getFakeId());
	    	roomService.save(s);
	    	return s;
    	}
    	else
    		return new Room();
        
    }
    @RequestMapping(value="/api/rooms",method = RequestMethod.POST, produces = "application/json")
    public Room createRoom(@RequestBody Room s){
    	List<Room> x = roomService.findAll();
    	int newID;
    	if(x.size() == 0)
    		newID = 1;
    	else 
    		newID = x.get(x.size()-1).getRoomId()+1;

    	Sensor a = sensorRepository.getByFakeId(Integer.parseInt(s.getId()));

    	if (a == null)
    		return new Room();

    	Room r = new Room(newID, s.getName(), s.getLocation(), Integer.parseInt(s.getId()));
    	roomService.save(r);
    	r.setId("" + r.getRoomId());
    	
    	return r;
        
    }
    
    
    @RequestMapping(value="/api/rooms/{id}",method = RequestMethod.DELETE, produces = "application/json")
    public Room delRoom(@PathVariable(value="id") int id){
    	roomService.deleteByRoomId(id);
    	return new Room();
        
    }

}
