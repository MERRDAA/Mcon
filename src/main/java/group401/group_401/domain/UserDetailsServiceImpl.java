//package group401.group_401.domain;
//
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import group401.group_401.repositories.UserRepository;
//import static java.util.Collections.emptyList;
//
//@Service
//public class UserDetailsServiceImpl implements UserDetailsService {
//	
//    private UserRepository applicationUserRepository;
//
//    public UserDetailsServiceImpl(UserRepository applicationUserRepository) {
//        this.applicationUserRepository = applicationUserRepository;
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//    	group401.group_401.domain.User applicationUser = applicationUserRepository.getByUsername(username);
//        if (applicationUser == null) {
//            throw new UsernameNotFoundException(username);
//        }
//        return new User(applicationUser.getUsername(), applicationUser.getPass(), emptyList());
//    }
//}
