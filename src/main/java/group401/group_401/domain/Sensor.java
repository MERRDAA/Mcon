package group401.group_401.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sound_records")
public class Sensor implements Serializable {

	@Id
    private String id;
	private int fakeId;
	private String type;
	private String sid;

	public Sensor() {
		super();
	}
	
	public Sensor(String type, String sid) {
		super();
		this.type = type;
		this.sid = sid;
	}
	
	public Sensor(int fakeId, String type, String sid) {
		super();
		this.fakeId = fakeId;
		this.type = type;
		this.sid = sid;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public int getFakeId() {
		return fakeId;
	}


	public void setFakeId(int fakeId) {
		this.fakeId = fakeId;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getSid() {
		return sid;
	}


	public void setSid(String sid) {
		this.sid = sid;
	}

	@Override
	public String toString() {
		return "Sensor [id=" + id + ", fakeId=" + fakeId + ", type=" + type + ", sid=" + sid + "]";
	}
	
	

}
