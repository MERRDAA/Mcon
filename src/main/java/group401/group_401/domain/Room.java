package group401.group_401.domain;



import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "rooms")
public class Room implements Serializable {
    
	@Id
    private String id;
	private int roomId;
	private String name;
	private String location;
    private int sensor;
    
	public Room() {
		super();
	}
    
	public Room(int roomId, String name, String location, int sensor) {
		super();
		this.roomId = roomId;
		this.name = name;
		this.location = location;
		this.sensor = sensor;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public int getRoomId() {
		return roomId;
	}


	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public int getSensor() {
		return sensor;
	}


	public void setSensor(int sensor) {
		this.sensor = sensor;
	}


	@Override
	public String toString() {
		return "Room [id=" + id + ", roomId=" + roomId + ", name=" + name + ", location=" + location + ", sensor="
				+ sensor + "]";
	}

    
	
	
	
    
    
}



