package listener;
import org.slf4j.Logger;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

import java.security.Timestamp;
import java.sql.Time;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.kafka.support.KafkaHeaders;

import group401.group_401.domain.Sensor;

@EnableScheduling
public class KafkaTopicListener {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private int alertValue;

	@Autowired
	private SimpMessagingTemplate template;

	@KafkaListener(id="realTime", topics = "401_realTime")
	public void noisyRooms(String message, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key) throws Exception {
		logger.info("Real Time Topic: " + key + ",value: " + message);

		/*Instant instant = Instant.now();
		long timeStampSeconds = instant.getEpochSecond();
		JSONObject jsonString = new JSONObject()
                .put("sensor", key )
                .put("value", message)
                .put("timestamp", timeStampSeconds);

		logger.info(jsonString.toString());*/
		if (key.equals("luisinho"))
			realTimeValues(message);

	}

	@KafkaListener(id="alert", topics = "401_alerts")
	public void quietlyRooms(String message, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key) throws Exception {
		logger.info("Alert Topic: " + key + ",value: " + message);
		alertValue = Integer.parseInt(message);
		//alert(message);
	}

	public void alert(String payload) throws Exception {
		this.template.convertAndSend("/topic/alerts", payload);
	}

	public void realTimeValues(String payload) throws Exception {
		this.template.convertAndSend("/topic/realTime", payload );
	}
	public int getAlertValue() {
		return alertValue;
	}
}
