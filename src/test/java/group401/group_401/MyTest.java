package group401.group_401;

import org.junit.runners.MethodSorters;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import group401.group_401.repositories.SensorRepository;
import listener.KafkaTopicListener;



import java.nio.charset.Charset;
import java.time.Instant;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MyTest {


	private int value;
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@SuppressWarnings("unused")
	@Autowired
	private SensorRepository sensorService;

	@Autowired
	private KafkaTopicListener receiver;

	private String topic = "401_all";

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	private MockMvc mockMvc;

	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
				.findAny()
				.orElse(null);

		assertNotNull("the JSON message converter must not be null",
				this.mappingJackson2HttpMessageConverter);
	}

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }


	@Test
	public void a_Send() throws JSONException{
		Instant instant = Instant.now();
		long timeStampSeconds = instant.getEpochSecond();
		JSONObject obj = new JSONObject();
		obj.put("sensor", "branquinho");
		obj.put("volume", 0.8);
		obj.put("timestamp", timeStampSeconds);
		kafkaTemplate.send(topic, obj.toString());
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Test
	public void b_receive() throws JSONException{

		value =receiver.getAlertValue();
		assertEquals(80, value);
	}

	//@Test
	//public void c_ontextLoads() {
		//System.out.println("ENTROU NOS TESTES? ");
	//}

    @Test
    public void d_sensoresPost() throws Exception {
		JSONObject content = new JSONObject();
		content.put("type", "testeSom");
		content.put("sid", "SID");
        mockMvc.perform(post("/api/sensors").contentType(contentType)
        		.content(content.toString()))        		
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void e_sensoresFound() throws Exception {

		MvcResult result = mockMvc.perform(get("/api/sensors"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        JSONArray jsonarray = new JSONArray(content);
        String sensorID = jsonarray.getJSONObject(0).getString("fakeId");

		mockMvc.perform(get("/api/sensors/"+sensorID))
                .andExpect(status().is2xxSuccessful())
        		.andExpect(jsonPath("$.type", is("testeSom")));

		mockMvc.perform(delete("/api/sensors/"+sensorID))
                .andExpect(status().is2xxSuccessful());
    }



}