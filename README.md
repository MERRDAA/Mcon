## Requirements
- maven 3 (can follow this guide https://maven.apache.org/install.html)

## Build

Create image by executing `mvn install dockerfile:build`

    $ mvn install dockerfile:build 

For now the connection to kafka cluster is on host 172.17.0.4 and port 9092.
Be careful running other docker images because this features must remain the same.

The topic in use for consumer is "work".


## Deploy Main Consumer

Run docker image by executing `docker run -p 8080:8080 --rm --interactive registry.gitlab.com/merrdaa/mcon:latest`

    $ docker run -p 8080:8080 --rm --interactive registry.gitlab.com/merrdaa/mcon:latest

After that can go to web browser and on `http://localhost:8080` it will show the information in real time of the producer!

## Deploy Kafka

#### Deploy zookeeper

    $ docker run -d -p 2181:2181 --name zookeeper jplock/zookeeper

#### Deploy kafka

    $ docker run -d -p 9092:9092 --name kafka --link zookeeper:zookeeper ches/kafka


#### Host and port of zookeeper and kafka

    $ ZK_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' zookeeper)
    $ KAFKA_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' kafka)


#### Create a topic

    $ docker run --rm ches/kafka \
    $ kafka-topics.sh --create --topic 404_all --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181
    
    $ docker run --rm ches/kafka \
    $ kafka-topics.sh --create --topic 404_windows --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181
    
    $ docker run --rm ches/kafka \
    $ kafka-topics.sh --create --topic 404_alerts --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181

##### Produce message

For testing purposes you can run the folloing to send a message to toppic `404_all`

    $ docker run --rm --interactive ches/kafka \
    $ kafka-console-producer.sh --topic work --broker-list $KAFKA_IP:9092
